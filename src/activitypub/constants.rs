// https://www.w3.org/TR/activitypub/#server-to-server-interactions
pub const ACTIVITY_CONTENT_TYPE: &str = r#"application/ld+json; profile="https://www.w3.org/ns/activitystreams""#;

pub const AP_CONTEXT: &str = "https://www.w3.org/ns/activitystreams";
pub const AP_PUBLIC: &str = "https://www.w3.org/ns/activitystreams#Public";
